use ModernWays;
insert into Huisdieren (
Baasje,
Naam,
Leeftijd,
Soort
)
values 
('Vincent', 'Misty', '6', 'hond'),
('Christiane', 'Ming', '8', 'hond'),
('Esther', 'Bientje', '6', 'kat'),
('Jommeke', 'Flip','75', 'papegaai'),
('Villads', 'Berto','1','papegaai'),
('Bert','Ming','7','papegaai'),
('Thaïs','Suerta','2','hond'),
('Lyssa','Фёдор','1','hond');